//
//  MyDataController.h
//  CoreDataProgramming
//
//  Created by Aquiles Alfaro on 11/27/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface MyDataController : NSManagedObject

@end
